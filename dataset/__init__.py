# from .voc import VOCDetection
# from torchray.benchmark.datasets import get_dataset
from .torchray_benchmark_datasets import get_dataset
from .torchray_benchmark_datasets import VOCDetection as torchray_benchmark_datasets_VOCDetection
import torchvision
import torchray
def my_get_dataset(name=None,
                subset=None,
                transform=None,
                download=None,
                limiter=None,
                  difficult_txt=None):
    if name == 'voc_2007' and subset == 'test':
        voc_detection = torchvision.datasets.VOCDetection('.', year='2007', 
        image_set='test', download=True, 
        transform=transform, target_transform=None)
        '''
        return torchray.benchmark.datasets.VOCDetection('.',
                                                        '2007',
                                                        'test',
                                                        transform = transform,
                                                        download = False,
                                                        limiter=limiter)
        '''
        return torchray_benchmark_datasets_VOCDetection('.',
                                                        '2007',
                                                        'test',
                                                        transform = transform,
                                                        download = False,
                                                        limiter=limiter,
                                                       difficult_txt=difficult_txt)

    return get_dataset(name=name,subset=subset,transform=transform,download=download,limiter=limiter,difficult_txt=difficult_txt)

'''
# https://pytorch.org/docs/stable/_modules/torchvision/datasets/voc.html#VOCSegmentation
# voc_trainset = torchvision.datasets.VOCDetection('.', year='2012', image_set='train', download=True, transform=transformations, target_transform=None, transforms=None)
voc_testset = torchvision.datasets.VOCDetection('.', year='2007', image_set='test', download=True, transform=transformations, target_transform=None, transforms=None)
# voc_testset = VOCDetection('.', year='2007', image_set='test', download=True, transform=transformations, target_transform=None, transforms=None)
torchray.benchmark.pointing_game.PointingGameBenchmark(voc_testset, tolerance=15, difficult=False)
'''