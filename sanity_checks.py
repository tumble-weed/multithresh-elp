import torch
import torchvision
import numpy as np 
if 'transform':
  models = {
  'vgg19':{
    'imsize':(224,224),
    'mean':(0.485, 0.456, 0.406),
    'std':(0.229, 0.224, 0.225),
    # 'model':
  },
  'alexnet':{
    'imsize':(227,227),
    'mean':'notimplemented',
    'std':'notimplemented',

  },
  'resnet18':{
    'imsize':(224,224),
    'mean':(0.485, 0.456, 0.406),
    'std':(0.229, 0.224, 0.225),
  },

  }
  def get_transform(modelname):
    if modelname == 'vgg19':
      #----------------------------------------------
      imsize = models[modelname]['imsize']
      mean = models[modelname]['mean']
      std = models[modelname]['std']
      #----------------------------------------------
      preprocess = torchvision.transforms.Compose([torchvision.transforms.Resize(imsize),
                                          torchvision.transforms.ToTensor(),
                                          torchvision.transforms.Normalize(mean = mean,std=std)
                                          ])
    else:
      assert False, f'{modelname} not supported'
    return preprocess

#=========================================  
def get_default_model(modelname):

    if modelname == 'vgg19':
        model = torchvision.models.vgg19(pretrained=True)
        model.eval()
        model.cuda()        
    
    elif modelname == 'resnet18':
        model = torchvision.models.resnet18(pretrained=True)
        model.eval()
        model.cuda()

        

    elif modelname == 'alexnet':
        model = torchvision.models.alexnet(pretrained=True)
        model.eval()
        model.cuda()
        
    
    return model

def get_conv2d_keys(model):
  # print('check if torchvision.models.vgg19 exists?')
  # if not isinstance(model,torchvision.models.vgg19):
  #   assert f'{model.__class__} not supported'
  
  conv2d_keys = []
  for key in model.features._modules.keys():

    if(isinstance(model.features._modules[key],torch.nn.modules.Conv2d)):
      conv2d_keys.append(key)
  return conv2d_keys    
def randomize_layer(model,layer):
  
  conv2d_keys = get_conv2d_keys(model)
  # assert layer < 0
  # conv2d_key = conv2d_keys[-1*layer]
  conv2d_key = conv2d_keys[-1*layer -1]
  #............................................
  layer = model.features._modules[conv2d_key]
  #print(layer)
  in_channels = layer.in_channels
  out_channels = layer.out_channels
  kernel_size = layer.kernel_size
  stride = layer.stride
  padding = layer.padding
  #............................................
  model.features._modules[conv2d_key] = torch.nn.modules.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, stride=stride, padding=padding)
  #............................................

#=========================================  

def cascade_randomization(modelname = None, num_layers_from_last = None):  

  model = get_default_model(modelname)
  preprocess = get_transform(modelname)

  # num = -1*num_layers_from_last

  # for conv2d_key in conv2d_keys[num:]:
  # for layer in range(-1*num_layers_from_last,0,1):
  for layer in range(num_layers_from_last,-1,-1):
    # assert layer < 0
    randomize_layer(model,layer)
  print('any way to check if the layers have actually been randomized?')

  model.eval()
  model.cuda()
  return model, preprocess


def independent_randomization(modelname = None, layer = None):  
  model = get_default_model(modelname)
  preprocess = get_transform(modelname)
  randomize_layer(model,layer)
  model.eval()
  model.cuda()    
  return model,  preprocess






from PIL import Image
from skimage import transform
from matplotlib import cm
def batch_overlay(heat_maps,ims, model_imsize):
    #TODO Write code for converting a batch of heat mapps into cm.jet images, and overlay them onto the reference image
    heat_map_jet = list(map(lambda h:cm.jet(h),heat_maps))
    heat_map_jet = np.array(heat_map_jet)[:,:,:,:3]
    # heat_map_jet = np.transpose(heat_map_jet, (2,0,1,3))
    heat_map_jet_pil = list(map(lambda t: Image.fromarray(np.uint8(t*255)), heat_map_jet))

    saliency_overlayed = []
    for im_i,h in zip(ims,heat_map_jet_pil):
        ref_i_pil = Image.fromarray((transform.resize(im_i,model_imsize)*255).astype(np.uint8))
        s = Image.blend(ref_i_pil,h,alpha=0.5)
#         s = np.array(s)
        saliency_overlayed.append(s)
#     saliency_overlayed = np.array(saliency)
    return saliency_overlayed,heat_map_jet_pil