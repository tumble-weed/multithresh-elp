from matplotlib import pyplot as plt
def imshow(im,title=''):
    plt.figure()
    plt.imshow(im,vmin=0,vmax=1)
    plt.title(title)
    plt.show()
    pass

def plot(y,title=''):
    plt.figure()
    plt.plot(y)
    plt.title(title)
    plt.show()
    pass

def plotxy(x,y,title=''):
    plt.figure()
    plt.plot(x,y)
    plt.title(title)
    plt.show()
    pass

def plotmany(*y,labels=[],title=''):
    plt.figure()
    for label,yi in zip(labels,y):
        plt.plot(yi,label=label)
    plt.title(title)
    plt.legend()
    plt.show()
    pass

def plotmany2(*y,labels=[],title=''):
    plt.figure()
    for i,(label,yi) in enumerate(zip(labels,y)):
        if i >0:
            plt.twinx().plot(yi,label=label)
        else:
            plt.plot(yi,label=label)
    plt.title(title)
    plt.legend()
    plt.show()
    pass
