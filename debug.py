
import os
import time
class debug():
    def __init__(self):
        self.assets = {}
        pass    
debug = debug()

class Timer():
    timers = {}
    def __init__(self,name):
        self.name = name
        self.n_calls = 0
        self.total_time = 0
        self.avg_time = 0
        self.tic = None

        pass
    @classmethod
    def get(cls,name):
        if name not in cls.timers:
            timer = cls(name)
            cls.timers[name] = timer
        timer = cls.timers[name]
        return timer
    def __enter__(self):
        self.n_calls += 1
        self.tic = time.time()
        return self
    def __exit__(self,exec_type,exec_value,exec_tb):
        toc = time.time()
        elapsed = toc - self.tic
        self.total_time += elapsed
        self.avg_time = self.total_time/self.n_calls
        self.tic = None
        pass
#--------------------------------
def print_timers():
    #=======================================================================
    for name,tim in Timer.timers.items():
        print(f'{tim.name} had {tim.n_calls} using up a total of {tim.total_time} sec and avg of {tim.avg_time} per call')
#--------------------------------
def purge_create(d,PURGE=True):
    if PURGE:
        try:
            os.system(f'rm -rf {d}')
        except FileNotFoundError:
            pass
    os.makedirs(d)
    
flags = {
    'common_grid':True,

    
}