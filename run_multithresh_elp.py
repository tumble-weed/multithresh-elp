#%% first
import os
os.environ['CUDA_LAUNCH_BLOCKING']='1'
os.environ['CUDA_VISIBLE_DEVICES']='0'
# os.environ['LD_PRELOAD'] ='<jemalloc.so/tcmalloc.so>:$LD_PRELOAD'
# os.environ['LD_PRELOAD'] ='<jemalloc.so>:$LD_PRELOAD'
# import os;os.chdir('/root/evaluate-saliency-4/')
# import importlib
# import benchmark.setup_single_image ;importlib.reload(benchmark.setup_single_image)
# from benchmark.setup_single_image import setup_single_image
# single_image_assets = setup_single_image(
#     base_parameters = 'parameters.parameters_vgg19_voc2007',
#     relative_save_dir = 'debug_saliency',
#     image_name = '007500',
#     class_name = None)
# # ['model', 'ref_full', 'class_id', 'class_name', 'OPTIONS', 'PLOT', 'device', 'save_dir']
#%%
# %matplotlib notebook
# %matplotlib widget
# %matplotlib inline
import main     
main.run_from_filename('parameters_sample.py')
# %%
