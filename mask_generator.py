import torchray
import math
import torch
import torch.nn.functional as F
from matplotlib import pyplot as plt
tensor_to_numpy = lambda t:t.detach().cpu().numpy()
def faster_get_thresholds(
    schedule,
    n_ticks,
    tolerance = 1,
    max_binary_search_iters=100,
    get_area = lambda t,schedule:(schedule>t).float().sum(),
    eps = 1e-8,
    min_area = 0,
    max_area = None,
):  
    assert schedule.shape[0] == 1
    device = schedule.device
    #.............................................
    uq_vals_asc,ranks_lowas0,counts_asc= torch.unique(schedule.detach(),sorted=True,return_counts=True,return_inverse=True)
    uq_vals_desc = torch.flip(uq_vals_asc,[0])
    ranks_lowas0 = torch.flip(ranks_lowas0,[0])
    counts_desc = torch.flip(counts_asc,[0])
    cumsum_of_desc = counts_desc.cumsum(dim=0)
    points_larger_than = torch.zeros(cumsum_of_desc.shape[0]+1).to(device)
    points_larger_than[1:] = cumsum_of_desc
    percent_points_larger_than = points_larger_than/cumsum_of_desc[-1]
    #.............................................
    required_areas = torch.linspace(0.,1.,n_ticks).to(device)
    required_vs_available = (required_areas[:,None] - percent_points_larger_than[None,:])
    errors = required_vs_available = required_vs_available.abs()
    ix_of_closest = required_vs_available.argmin(dim=-1)
    if False:
        thresh_out = percent_points_larger_than[ix_of_closest]
    else:
        padded_uq_vals_desc = torch.zeros_like(percent_points_larger_than)
        padded_uq_vals_desc[1:] = uq_vals_desc
        padded_uq_vals_desc[0] = 1
    thresh_out = padded_uq_vals_desc[ix_of_closest]
    largest_error = errors.max()
    # import pdb;pdb.set_trace()
    return thresh_out,(errors<=tolerance).all(),largest_error,ix_of_closest,ranks_lowas0        


class MaskGenerator:
    r"""Mask generator.

    The class takes as input the mask parameters and returns
    as output a mask.

    Args:
        shape (tuple of int): output shape.
        step (int): parameterization step in pixels.
        sigma (float): kernel size.
        clamp (bool, optional): whether to clamp the mask to [0,1]. Defaults to True.
        pooling_mehtod (str, optional): `'softmax'` (default),  `'sum'`, '`sigmoid`'.

    Attributes:
        shape (tuple): the same as the specified :attr:`shape` parameter.
        shape_in (tuple): spatial size of the parameter tensor.
        shape_out (tuple): spatial size of the output mask including margin.
    """

    def __init__(self, shape, step, sigma, clamp=True, pooling_method='softmax'):
        # step = 1
        self.shape = shape
        self.step = step
        self.sigma = sigma
        self.coldness = 20
        self.clamp = clamp
        self.pooling_method = pooling_method

        assert int(step) == step

        # self.kernel = lambda z: (z < 1).float()
        self.kernel = lambda z: torch.exp(-2 * ((z - .5).clamp(min=0)**2))

        self.margin = self.sigma
        # self.margin = 0
        self.padding = 1 + math.ceil((self.margin + sigma) / step)
        self.radius = 1 + math.ceil(sigma / step)
        self.shape_in = [math.ceil(z / step) for z in self.shape]
        self.shape_mid = [
            z + 2 * self.padding - (2 * self.radius + 1) + 1
            for z in self.shape_in
        ]
        self.shape_up = [self.step * z for z in self.shape_mid]
        self.shape_out = [z - step + 1 for z in self.shape_up]

        self.weight = torch.zeros((
            1,
            (2 * self.radius + 1)**2,
            self.shape_out[0],
            self.shape_out[1]
        ))

        step_inv = [
            torch.tensor(zm, dtype=torch.float32) /
            torch.tensor(zo, dtype=torch.float32)
            for zm, zo in zip(self.shape_mid, self.shape_up)
        ]

        for ky in range(2 * self.radius + 1):
            for kx in range(2 * self.radius + 1):
                uy, ux = torch.meshgrid(
                    torch.arange(self.shape_out[0], dtype=torch.float32),
                    torch.arange(self.shape_out[1], dtype=torch.float32)
                )
                iy = torch.floor(step_inv[0] * uy) + ky - self.padding
                ix = torch.floor(step_inv[1] * ux) + kx - self.padding

                delta = torch.sqrt(
                    (uy - (self.margin + self.step * iy))**2 +
                    (ux - (self.margin + self.step * ix))**2
                )

                k = ky * (2 * self.radius + 1) + kx
                
                self.weight[0, k] = self.kernel(delta / sigma)
        # assert False
    def generate(self, mask_in,t=None,n_areas=None):
        r"""Generate a mask.

        The function takes as input a parameter tensor :math:`\bar m` for
        :math:`K` masks, which is a :math:`K\times 1\times H_i\times W_i`
        tensor where `H_i\times W_i` are given by :attr:`shape_in`.

        Args:
            mask_in (:class:`torch.Tensor`): mask parameters.

        Returns:
            tuple: a pair of mask, cropped and full. The cropped mask is a
            :class:`torch.Tensor` with the same spatial shape :attr:`shape`
            as specfied upon creating this object. The second mask is the same,
            but with an additional margin and shape :attr:`shape_out`.
        """
        mask0 = mask_in.clone()
        if True and 'thresholds on pmask':
            # n_areas = 40;print(f'hard coding n_areas to {n_areas}')
            required_t,ignore,ignore,ignore,ignore= faster_get_thresholds(
                    mask_in,
                    n_areas,
                    tolerance = 1,
                    max_binary_search_iters=100,
                    get_area = lambda t,schedule:(schedule>t).float().sum(),
                    eps = 1e-8,
                    min_area = 0,
                    max_area = None,
                )        
            assert required_t.ndim == 1
            mask_c = (mask_in >= required_t[:,None,None,None]).float()
            if False:
                print(required_t)
                print(mask_c.sum())
            mask_in = (mask_c - mask_in).detach() + mask_in
            # mask_in =torch.cat([mask_in for _ in range(n_areas)],dim=0)

        mask = F.unfold(mask_in,
                        (2 * self.radius + 1,) * 2,
                        padding=(self.padding,) * 2)
        
        mask = mask.reshape(
            len(mask_in), -1, self.shape_mid[0], self.shape_mid[1])
        mask = F.interpolate(mask, size=self.shape_up, mode='nearest')
        mask = F.pad(mask, (0, -self.step + 1, 0, -self.step + 1))
        '''
        if t is not None:
            assert t.ndim == 1
            # t = torch.zeros_like(t)
            t = torch.linspace(0,1,t.shape[0]).to(t.device)
            # import pdb;pdb.set_trace()
            mask_t = mask - t[:,None,None,None]
        '''
        mask = self.weight * mask
        # assert False
        if self.pooling_method == 'sigmoid':
            if self.coldness == float('+Inf'):
                mask = (mask.sum(dim=1, keepdim=True) - 5 > 0).float()
            else:
                mask = torch.sigmoid(
                    self.coldness * mask.sum(dim=1, keepdim=True) - 3
                )
        elif self.pooling_method == 'softmax':
            if self.coldness == float('+Inf'):
                mask = mask.max(dim=1, keepdim=True)[0]
            else:
                # import pdb;pdb.set_trace()
                mask = (
                    mask * F.softmax(self.coldness * mask, dim=1)
                ).sum(dim=1, keepdim=True)

        elif self.pooling_method == 'sum':
            mask = mask.sum(dim=1, keepdim=True)
        else:
            assert False, f"Unknown pooling method {self.pooling_method}"
        m = round(self.margin)
        if self.clamp:
            mask = mask.clamp(min=0, max=1)
        #========================================================
        # simple thresholding based masking
        # assert t.ndim == 1
        # t = torch.linspace(0,1,t.shape[0]).to(t.device)
        # mask_c = torch.clamp(mask - t[:,None,None,None],0,None)
        if False and 'thresholds on mask':
            n_areas = 10;print('hard coding n_areas to 10')
            required_t,ignore,ignore,ignore,ignore= faster_get_thresholds(
                    mask,
                    n_areas,
                    tolerance = 1,
                    max_binary_search_iters=100,
                    get_area = lambda t,schedule:(schedule>t).float().sum(),
                    eps = 1e-8,
                    min_area = 0,
                    max_area = None,
                )
            assert required_t.ndim == 1
            mask_c = (mask >= required_t[:,None,None,None]).float()
            mask = (mask_c - mask).detach() + mask
        #========================================================
        cropped = mask[:, :, m:m + self.shape[0], m:m + self.shape[1]]
        #-----------------------------------------------------------
        # print('see shape of mask, can area be controlled here');
        
        global ncalls
        if 'ncalls' not in globals():
            ncalls = 0 
        
        if (ncalls % 100) == 0:
            # import pdb;pdb.set_trace()
            import plotters
            # for i in range(cropped.shape[0]):
            #     plotters.imshow(tensor_to_numpy(mask[i,0]),
            #     title=f'{ncalls},{i},area:{mask[i,0].sum().item()}')
            plotters.imshow(tensor_to_numpy(mask[mask.shape[0]//2,0]),
                title=f'{ncalls},{mask.shape[0]//2},area:{mask[mask.shape[0]//2,0].sum().item()}')
            plotters.plot(tensor_to_numpy(mask.sum(dim=(1,2,3))),
            title=f'{ncalls},area')
            # import pdb;pdb.set_trace()
        ncalls += 1
        #----------------------------------------------------------__

        return cropped, mask

    def to(self, dev):
        """Switch to another device.

        Args:
            dev: PyTorch device.

        Returns:
            MaskGenerator: self.
        """
        self.weight = self.weight.to(dev)
        return self
