from torchvision.models.resnet import ResNet
# from torchvision.models.resnet import  make_layers as resnet_make_layers
# from torchvision.models.resnet import cfgs as resnet_cfgs
from torchvision.models.resnet import load_state_dict_from_url  as resnet_load_state_dict_from_url
# try:
#     from torch.hub import load_state_dict_from_url
# except ImportError:
#     from torch.utils.model_zoo import load_url as load_state_dict_from_url
from torchvision.models.resnet import model_urls as model_urls_resnet
from typing import Union, List, Dict, Any, cast, Type
from torchvision.models.resnet import (BasicBlock, Bottleneck)

class MyResNet(ResNet):
    def forward(self, x):
        x0 = x
        # Same as original, but skip flatten layer.
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        x = self.avgpool(x)
        x = self.fc(x)

        return x    
#---------------------------------
import torch
def _resnet(
    arch: str,
    block: Type[Union[BasicBlock, Bottleneck]],
    layers: List[int],
    pretrained: bool,
    progress: bool,
    **kwargs: Any
) -> MyResNet:
#     import pdb;pdb.set_trace()
    model = MyResNet(block, layers, **kwargs)
    model.avgpool = torch.nn.AvgPool2d(kernel_size=(7,7),stride=(1,1))
    print('replacing resnet50 adaptiveavgpool with avgpool'); import time;time.sleep(5)
    if pretrained:
        state_dict = resnet_load_state_dict_from_url(model_urls_resnet[arch],
                                              progress=progress)
        model.load_state_dict(state_dict)
    return model
#---------------------------------
def resnet50(pretrained: bool = False, progress: bool = True, **kwargs: Any) -> ResNet:
    r"""ResNet-50 model from
    `"Deep Residual Learning for Image Recognition" <https://arxiv.org/pdf/1512.03385.pdf>`_.
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
        progress (bool): If True, displays a progress bar of the download to stderr
    """
#     import pdb;pdb.set_trace()
    return _resnet('resnet50', Bottleneck, [3, 4, 6, 3], pretrained, progress,
                   **kwargs)
#---------------------------------
