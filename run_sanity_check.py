# https://stackoverflow.com/questions/51186921/how-to-patch-a-python-3-module-on-first-import
import builtins
original_import = builtins.__import__

def custom_import(*args, **kw):
    module = original_import(*args, **kw)

    if (module.__name__ == "jigsaw" 
        and not getattr(module, "patch_is_performed", False)):

        import jigsaw2 as module
        print('patching jigsaw2')
#         import jigsaw_pyr_blend as module
        module.patch_is_performed = True
        
    if (module.__name__ == "metrics" 
        and not getattr(module, "patch_is_performed", False)):

#         import metrics2 as module
#         print('patching metrics2')

        module.patch_is_performed = True

    return module

builtins.__import__ = custom_import

#===================================================================
from benchmark.sanity_checks import *
import torch
from skimage import io
import os
from PIL import Image
import numpy as np
from cnn import get_vgg_transform, get_target_id
from utils import get_image_tensor
from benchmark.benchmark_utils import create_im_save_dir
import pickle
from matplotlib import pyplot as plt
import glob
# from sanity_checkks import batc
tensor_to_numpy = lambda t:t.detach().cpu().numpy()
'''
def get_images(images,img_dir,categories):
  refs = []
  ims = []
  for img, class_of_interest in zip(images, categories):
    im = io.imread(os.path.join(img_dir, img))
    if len(im.shape) == 2:
      im = np.stack((im,)*3, axis=-1)
    print(im.shape)
    ims.append(im)
    im_pil = Image.fromarray(im)
    ref = preprocess(im_pil).unsqueeze(0)
    print(ref.shape)
    ref = ref.cuda()
    refs.append(ref)
  return refs
'''
def run_independent_randomization_panel(modelname,refs,categories,get_saliency,get_get_saliency=None,display = False):
    independent_random_output_binaries = {}
    n_layers = 17
    if modelname != 'vgg19':
        print('n_layers is hardcoded to vgg19')
        assert False
    #----------------------------------------------
    ref_shapes = []
    out = {}
    # for i in range(1, n_layers):
    for i in range(0, n_layers):
        print(f'i ---> {i}')
        if False:
          independent_random_output_binaries[i] = []
        model,  preprocess = independent_randomization(modelname = modelname, layer = i)
        # last_spatial_layer,hooked_last_spatial,bwdhooked_last_spatial = prepare_model_for_gradcam(modelname,model)
        # L_c_nps, heat_maps, ref_scores = get_saliency(model, final_refs , categories, model_imsize, last_spatial_layer)
        # assert False
        # print(refs[0].shape)
        ref_shapes.append(refs[0].shape)
        if callable(get_get_saliency):
            get_saliency =  get_get_saliency()
        heat_maps = get_saliency(model,refs[0],categories[0])
        # if i == 2:
        #   assert False
        if False:
            from cnn import denormalize_vgg
            saliency_overlayed, pil_heat_map_jet = batch_overlay(heat_maps, denormalize_vgg(refs[0]), refs[0].shape[-2:])
        #----------------------------------------------
        if False:
          for heat_map in heat_maps:
              #independent_random_output_binary = np.mean(np.asarray(heat_map), axis=2).astype('uint8')
              #independent_random_output_binaries[i].append(independent_random_output_binary)
              independent_random_output_binaries[i].append(heat_map)
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if display:     
          plt.figure()
          plt.imshow(tensor_to_numpy(heat_maps[0,0]),cmap='hot')
          plt.title(f'{i}')
          plt.gcf().canvas.draw()
          plt.show()
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        #----------------------------------------------
        del model
        torch.cuda.empty_cache()
        out[i] = heat_maps
    return out
  #----------------------------------------------


def run_cascade_randomization_panel(modelname,refs,categories,get_saliency,display=False,save=None):
    cascade_random_output_binaries = {}
    # n_layers = 17
    model = get_default_model(modelname)
    conv2d_keys = get_conv2d_keys(model)
    n_layers = len(conv2d_keys)
    if modelname != 'vgg19':
        print('n_layers is hardcoded to vgg19')
    out = {}
    # for i in range(1, n_layers):
    # for i in range(n_layers-1, n_layers):
    for i in range(0, n_layers):
        print(f'i ---> {i}')
        cascade_random_output_binaries[i] = []
        model,preprocess = cascade_randomization(modelname = modelname, num_layers_from_last = i)
        # last_spatial_layer,hooked_last_spatial,bwdhooked_last_spatial = prepare_model_for_gradcam(modelname,model)
        # L_c_nps, heat_maps, ref_scores = get_saliency(model, final_refs , categories, model_imsize, last_spatial_layer)
        #saliency_overlayed, pil_heat_map_jet = batch_overlay(heat_maps, ims, model_imsize)
        #import pdb; pdb.set_trace()
        heat_maps = get_saliency(model,refs[0],categories[0])
        for heat_map in heat_maps:
            #cascade_random_output_binary = np.mean(np.asarray(heat_map), axis=2).astype('uint8')
            #cascade_random_output_binaries[i].append(cascade_random_output_binary)
            cascade_random_output_binaries[i].append(heat_map)
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if display:
          plt.figure()
          plt.imshow(tensor_to_numpy(heat_maps[0,0]),cmap='hot')
          plt.title(f'{i}')
          plt.gcf().canvas.draw()
          plt.show()
        out[i] = heat_map
        if save is not None:
          save(i,heat_map)
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      
        del model
        torch.cuda.empty_cache()
        # print('breaking after 1');break

    return out



def get_closure(P):
    from main import part_discovery
    def closure(model,ref,target_id):
        trends,patch_theta0,importances = part_discovery(
                  # P.n_patches,P.patch_init_mode,
                  # P.sigma,P.span,
                  P.patch_options,
                  # P.n_patches_per_image,P.n_images_in_batch,
                  P.jigsaw_options,
                  ref,model,target_id,
                  P.niter1,P.niter2,P.lr,P.pixel_jitter,
                  # P.patch_importance_momentum,
                  P.patch_correction_mode,
                  P.evaluation_options,
                  # P.evaluate_every,P.visualize_every,
                  # P.e_lambda,P.o_lambda,P.r_lambda,
                  P.loss_options,
                  P.importance_options,
                  P.non_lin,
                  P.patch_lim_mode,
                  device=P.device)
        from jigsaw import place_patches_at_locations
        from evaluate import realign_patch_centers_
        from patch import create_gaussian_masks,extract_masked_patches
        from model import patch_centers_
        from model import get_patch_theta
        #========================================================
        grid_size = ref.shape[-2:]
        lim_mode = 'full'
        patch_theta = get_patch_theta(patch_theta0,grid_size,P.patch_options['span'],non_lin = P.non_lin,lim_mode=lim_mode)
        #========================================================
        patch_centers = patch_centers_(patch_theta,grid_size,P.patch_options['span'])
        patch_masks = create_gaussian_masks(patch_centers,P.patch_options['span'],P.patch_options['sigma'],grid_size,normalize_mask=P.patch_options['normalize_mask'],device=P.device)
        patches = extract_masked_patches(ref,patch_masks,patch_centers,P.patch_options['span'],masking_mode=P.patch_options['masking_mode'])
            
        # patch_centers = patch_centers_(patch_theta,grid_size,P.span)
        # patch_masks = create_gaussian_masks(patch_centers,P.span,P.sigma,grid_size,device=P.device)
        # patches = extract_masked_patches(ref,patch_masks,patch_centers,
        # P.span)      
        #========================================================
        ###############################################
        import torch
        from patch import get_relative_patch_centers


        new_patch_centers = realign_patch_centers_(ref.shape[-2:],patches,patch_centers,P.patch_options['span'])
        relative_patch_centers =  get_relative_patch_centers(patch_centers,P.patch_options['span'],ref.shape[-2:])
        ###############################################    
        new_patch_centers = realign_patch_centers_(ref.shape[-2:],patches,patch_centers,P.patch_options['span'])
        importance_patches = [pm * pi for pm,pi in zip(patch_masks,importances.importances)]
        # heat_map = place_patches_at_locations(ref.shape[-2:],importance_patches,new_patch_centers,P.span)
        heat_map = place_patches_at_locations(ref.shape[-2:],importance_patches,new_patch_centers,relative_patch_centers,P.patch_options['span'])
        # heat_map = place_patches_at_locations(ref.shape[-2:],patches,new_patch_centers,P.span)
        # heat_map_ = tensor_to_numpy(heat_map)[0,0]
        # return heat_map_
        return heat_map
    return closure
def main():
    import parameters.parameters4 as P
    get_saliency = get_closure(P)
    get_get_saliency = lambda P=P:get_closure(P)
    modelname = 'vgg16'
    # preprocess = get_transform(modelname)
    # refs = [preprocess()]
    refs = [get_image_tensor(P.impath,size=(224,)).to(P.device)]
    # assert False
    target_ids = [get_target_id(P.class_name)]        
    ######################################################
    def get_save(sanityname,impath,classname,target_id):
      im_save_dir = create_im_save_dir(experiment_name=f'sanity-check-{sanityname}-{modelname}',impath=impath)  
      def save(i,heat_map):
        savename = os.path.join(im_save_dir,f'{i}_{classname}{target_id}.pkl')
        sanity_data = {
          i:tensor_to_numpy(heat_map),
          'modelname' : modelname,
          'refs' : [tensor_to_numpy(r) for r in refs],
          'target_ids' : target_ids,
          'classname':classname,
          }
        with open(savename,'wb') as f:
          pickle.dump(sanity_data,f)                    
          pass
      return save
    if False:
      sanityname = 'independent'
      save_independent = get_save(sanityname,P.impath,P.class_name,target_ids[0])      
      independent_randomization_results = run_independent_randomization_panel(modelname,refs,target_ids,get_saliency,get_get_saliency = None,save=save_independent)
      assert False
      
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~          
    if True:
      sanityname = 'cascade'
      save_cascade = get_save(sanityname,P.impath,P.class_name,target_ids[0])
      cascade_randomization_results = run_cascade_randomization_panel(modelname,refs,target_ids,get_saliency,save=save_cascade)   
      # cascade_randomization_results.update(dict(
      #     modelname = modelname,
      #     refs = [tensor_to_numpy(r) for r in refs],
      #     target_ids = target_ids,
      #   ),save=save_cascade)
      
    ######################################################
    # im_save_dir = create_im_save_dir(experiment_name=f'sanity-check-{sanityname}{modelname}',impath=P.impath)  
    # savename = os.path.join(im_save_dir,f'{classname}{target_id}.pkl')
    # with open(savename,'wb') as f:
    #     pickle.dump(saliency_data,f)            
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      
from benchmark import settings
def load(impath=None,modelname='vgg19',sanityname='cascade'):
  import pickle
  impath = impath.rstrip(os.path.sep)
  impath = os.path.basename(impath)
  imroot = impath.split('.') # remove extension
  imroot = imroot[0] 
  assert imroot == 'samoyed1'
  # im_save_dir = create_im_save_dir(experiment_name=f'sanity-check-{sanityname}-{modelname}',impath=impath)  
  im_save_dir = os.path.join(settings.RESULTS_DIR,f'sanity-check-{sanityname}-{modelname}',imroot)
  loadnames = glob.glob(os.path.join(im_save_dir,'*.pkl'))
  
  for loadname in loadnames:
    with open(loadname,'rb') as f:
      loaded = pickle.load(f)
    # assert False
  # loadname = '/root/evaluate-saliency-4/jigsaw/benchmark/results/sanity-check-cascade-vgg19/samoyed1/1_samoyed258.pkl'
if __name__ == '__main__':
  import parameters.parameters4 as P
  # P.niter1 = 4;print('setting niter1=4 for testing')
  main()
  if False:
    loaded = load(impath=P.impath,modelname='vgg19',sanityname='cascade')
    import pdb;pdb.set_trace()
    print('hi')