#done
# rclone sync jigsaw/benchmark/results/gradcam-vgg19/ aniketsinghresearch-gdrive:competing-saliency/gradcam-vgg19
# rclone sync jigsaw/benchmark/results/smoothgrad-vgg19/ aniketsinghresearch-gdrive:competing-saliency/smoothgrad-vgg19
#=================================================================
# TODO
# rclone sync -P jigsaw/benchmark/results/fullgrad-vgg19/ aniketsinghresearch-gdrive:competing-saliency/fullgrad-vgg19 --compress-level 9
#=================================================================
# rclone sync -P /root/evaluate-saliency-4/jigsaw/benchmark/results/gradients-vgg19/ aniketsinghresearch-gdrive:competing-saliency/gradients-vgg19 --compress-level 9
# rclone sync -P /root/evaluate-saliency-4/jigsaw/benchmark/results/inputXgradients-vgg19/ aniketsinghresearch-gdrive:competing-saliency/inputXgradients-vgg19 --compress-level 9
# rclone sync -P /root/evaluate-saliency-4/jigsaw/benchmark/results/integrated-gradients-vgg19/ aniketsinghresearch-gdrive:competing-saliency/integrated-gradients-vgg19 --compress-level 9
#=================================================================
# tar -zcvf /root/evaluate-saliency-4/jigsaw/benchmark/results/metrics_vgg16.tgz /root/evaluate-saliency-4/jigsaw/benchmark/results/metrics  
# rclone sync -P /root/evaluate-saliency-4/jigsaw/benchmark/results/metrics_vgg16.tgz aniketsinghresearch-gdrive:competing-saliency/metrics_vgg16.tgz --compress-level -1
#=================================================================
rclone sync -P /root/evaluate-saliency-4/jigsaw/benchmark/results/on-image-jigsaw-saliency-vgg16/ aniketsinghresearch-gdrive:competing-saliency/on-image-jigsaw-saliency-vgg16 --compress-level -1
rclone sync -P /root/evaluate-saliency-4/jigsaw/benchmark/results/on-zeros-jigsaw-saliency-vgg16/ aniketsinghresearch-gdrive:competing-saliency/on-zeros-jigsaw-saliency-vgg16 --compress-level -1
rclone sync -P /root/evaluate-saliency-4/jigsaw/benchmark/results/jigsaw-saliency-vgg16/ aniketsinghresearch-gdrive:competing-saliency/on-image-50patch-100iter-jigsaw-saliency-vgg16/ --compress-level -1
####################################################################
# For Downloading
####################################################################
# rclone sync -P aniketsinghresearch-gdrive:competing-saliency/metrics_vgg16.tgz /root/evaluate-saliency-4/jigsaw/benchmark/results/
# tar -xvzf /root/evaluate-saliency-4/jigsaw/benchmark/results/metrics_vgg16.tgz
# mv /root/evaluate-saliency-4/jigsaw/benchmark/results/root/evaluate-saliency-4/jigsaw/benchmark/results/metrics/* .

# rclone sync -P aniketsinghresearch-gdrive:competing-saliency/vgg16-results.tar.gz /root/evaluate-saliency-4/jigsaw/benchmark/results/
# tar -xvzf /root/evaluate-saliency-4/jigsaw/benchmark/results/vgg16-results.tar.gz
