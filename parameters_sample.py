import torch
import os
meta_OPTIONS = {
    'device': 'cuda' if torch.cuda.is_available() else 'cpu',
    #=======================================================================
    # MAKE EXPERIMENTS
    #=======================================================================
    'series' : 'attribution_benchmarks',
    }
meta_OPTIONS['series_dir'] =  os.path.join('data', meta_OPTIONS['series'])
meta_OPTIONS.update({
    'log' : 0,
    'seed' : 0,
    'n_data_chunks' : 200,
    'data_chunk_ix' : 0,
    'n_mp_chunks' : 1,
    'n_gpu':-1,

    'datasets' : [
        'voc_2007',
        # 'coco',
    ],

    'archs' : [
        'vgg16',
                        # 'resnet50',
    ],

    'methods' : [
        'multithresh_extremal_perturbation',
        # 'center',
        # 'contrastive_excitation_backprop',
        # 'deconvnet',
        # 'excitation_backprop',
        # 'grad_cam',
        # 'gradient',
        # 'guided_backprop',
        # 'rise',
        # 'extremal_perturbation',
    ],

})
#=======================================================================
# SETUP
#=======================================================================
import settings as settings
SAVE_PARENT = settings.RESULTS_DIR
SAVE_PARENT = '.'
meta_OPTIONS['PURGE'] = True
meta_OPTIONS['save_dir'] = f"results_{meta_OPTIONS['archs'][0]}_{meta_OPTIONS['datasets'][0]}"
meta_OPTIONS['save_dir'] = os.path.join(settings.RESULTS_DIR,meta_OPTIONS['save_dir'] + '_no_lr_scheduler2')
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# reproducibility
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def for_reproducibility(device):
    if device == 'cuda':
        torch.backends.cudnn.benchmark =True
        torch.backends.cudnn.enabled = True
for_reproducibility(meta_OPTIONS['device'])

'''
changes:
    #- n_patches_per_image = 1
    #- spatial transformer use_scales
'''
OPTIONS = {
    'data':{'difficult_txt':None,'NO_RESIZE':False},
    'part_discovery':dict(
    save_dir = None,
    #..............................................
    patch_options = dict(n_patches = 9,patch_init_mode = 'uniform',sigma = 81,span = 81,normalize_mask=False,masking_mode='multiply'),
    jigsaw_options = dict(n_patches_per_image = 1, 
        size = (224,224), 
    # jigsaw_center_mode = 'fong',
    # jigsaw_center_mode = 'center',
    jigsaw_center_mode = 'random',
    patch_selection_mode='random',
    # patch_selection_mode='variance',
    # paste_on='image-smoothed',
    paste_on='zeros'
    # paste_on='image'
    ),
    niter1 = 200,
    niter2 = 100,
    lr = 1*1e0/224,
    patch_correction_mode = 'valid',
    pixel_jitter = 0,
    loss_options = dict(e_lambda = 1,o_lambda = 0,r_lambda  = 0e-5,n_images_in_batch = 5,form='importance',occlusion_mode='expose'),
    evaluation_options = dict(
        evaluate_every = 10,visualize_every = 10,no_standalone=True
        ),
    # importance_options = dict(noise_lr=0.1,initial_importance=0,noise_logit=10,std=1,patch_importance_momentum = 0.9),
    importance_options = dict(noise_lr=0.001,initial_importance=0,noise_logit=1,std=1,patch_importance_momentum = 0.9,
    # update_strategy='point'
    update_strategy='bayes'
    ),
    non_lin = None,
    patch_lim_mode = 'valid',
    device = meta_OPTIONS['device'],
)
    
}