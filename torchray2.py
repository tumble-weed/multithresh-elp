# import setup
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import GPUtil
import os
import torch
from termcolor import colored
import numpy as np
import pickle
import pandas as pd
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from image_transforms import get_transform
from torch.utils.data import DataLoader, Subset
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from torchray.attribution.common import get_pointing_gradient
from torchray.benchmark.pointing_game import PointingGameBenchmark
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from dataset import my_get_dataset
from dataset.torchray_benchmark_datasets import _parse_difficult_txt

from benchmark_utils import Timer
# import saliency as my_saliency
# from saliency import run_my_saliency
# from saliency_with_TS import run_my_saliency
# import saliency_with_TS
# import visualize
# from utils import Timer
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Different Attribution Methods
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from torchray.attribution.deconvnet import deconvnet
from torchray.attribution.excitation_backprop import contrastive_excitation_backprop
from torchray.attribution.excitation_backprop import excitation_backprop
from torchray.attribution.excitation_backprop import update_resnet
from torchray.attribution.grad_cam import grad_cam
from torchray.attribution.gradient import gradient
from torchray.attribution.guided_backprop import guided_backprop
from torchray.attribution.rise import rise
import torchray.attribution.extremal_perturbation as elp

from tqdm import tqdm_notebook as tqdm
import multithresh_elp
tensor_to_numpy = lambda t:t.detach().cpu().numpy()
# if setup.IN_NOTEBOOK:
#     from tqdm import tqdm_notebook as tqdm
# else:
#     from tqdm import tqdm
#=======================================================================
# EXPERIMENT EXECUTOR
#=======================================================================
class ExperimentExecutor():
    def get_images_for_chunk(self,chunk):
#         assert self.data.images == sorted(self.data.images)!
        sorted_ixs = sorted(range(len(self.data.images)), key=lambda k: self.data.images[k])
        imagenames = [self.data.images[ix] for ix in sorted_ixs]
        total_n_images = len(imagenames)
        chunk_size = int(np.ceil(total_n_images/ self.total_n_chunks))
        imagenames = imagenames[chunk_size*chunk:chunk_size*(chunk+1)]
        image_ixs = range(total_n_images)[chunk_size*chunk:chunk_size*(chunk+1)]
        image_ixs = [sorted_ixs[ix] for ix in image_ixs]
        return imagenames,image_ixs
    def __init__(self, experiment, chunk=None, debug=0, log=0, seed=0,device_preference='cuda',
                total_n_chunks=None,only_difficult_txt=None,pid=None,OPTIONS={}):
        self.experiment = experiment
        self.device = None
        self.device_preference = device_preference
        self.model = None
        self.data = None
        self.loader = None
        self.pointing = None
        self.pointing_difficult = None
        self.only_difficult_txt = only_difficult_txt
        self.difficulty = _parse_difficult_txt(self.only_difficult_txt)
        self.debug = debug
        self.log = log
        self.seed = seed
        self.total_n_chunks = total_n_chunks
        self.current_ix = -1
        self.pid = pid
        self.start_from = None
        self.OPTIONS = OPTIONS
        if self.experiment.arch == 'vgg16':
            self.gradcam_layer = 'features.29'  # relu before pool5
            self.saliency_layer = 'features.23'  # pool4
            self.contrast_layer = 'classifier.4'  # relu7
        elif self.experiment.arch == 'resnet50':
            self.gradcam_layer = 'layer4'
            self.saliency_layer = 'layer3'  # 'layer3.5'  # res4a
            self.contrast_layer = 'avgpool'  # pool before fc layer
        else:
            assert False

        if self.experiment.dataset == 'voc_2007':
            subset = 'test'
            import dataset.voc_classes as class_names
            # import benchmark.dataset.voc_classes as class_names
        elif self.experiment.dataset == 'coco':
            subset = 'val2014'
            import dataset.coco_classes as class_names
            # import benchmark.dataset.coco_classes as class_names
        else:
            assert False
        self.class_names = class_names.class_names
        # Load the model.
        if self.experiment.method in ["rise",
                                      
                                     ]:
            input_size = (224, 224)
        else:
            input_size = 224    
        # input_size = (224,224)
        if OPTIONS['data']['NO_RESIZE']:
            input_size = None
        transform = get_transform(size=input_size,
                                  dataset=self.experiment.dataset)
        # import pdb;pdb.set_trace()
        self.data = my_get_dataset(name=self.experiment.dataset,
                                subset=subset,
                                transform=transform,
                                download=True,
                                limiter=None,
                                  difficult_txt=OPTIONS['data']['difficult_txt'])

        # Get subset of data. This is used for debugging and for
        # splitting computation on a cluster.
        if chunk is None:
            chunk = self.experiment.chunk

        if isinstance(chunk, dict):
            dataset_filter = chunk
            chunk = []
            if 'image_name' in dataset_filter:
                for i, name in enumerate(self.data.images):
                    if dataset_filter['image_name'] in name:
                        chunk.append(i)

            print(f"Filter selected {len(chunk)} image(s).")
        #------------------------------------
        if isinstance(self.total_n_chunks,int):
            assert isinstance(chunk,int)
            imagenames,image_ixs = self.get_images_for_chunk(chunk)    
            chunk = image_ixs
            print(f"Filter selected {len(chunk)} image(s).")
        #------------------------------------        
         # Limit the chunk to the actual size of the dataset.
        if chunk is not None:
            chunk = list(set(range(len(self.data))).intersection(set(chunk)))
        
        # Extract the data subset.
        chunk = Subset(self.data, chunk) if chunk is not None else self.data
        # import pdb;pdb.set_trace()
        # Get a data loader for the subset of data just selected.
        self.loader = DataLoader(chunk,
                                 batch_size=1,
                                 shuffle=False,
                                 num_workers=0,
                                 collate_fn=self.data.collate)

        self.pointing = PointingGameBenchmark(self.data, difficult=False)
        self.pointing_difficult = PointingGameBenchmark(
            self.data, difficult=True)
        
        # self.pointing.evaluate_hooks.append(!)
        # self.pointing.evaluate_hooks.append(!)
        self.data_iterator = iter(self.loader)
        self.df_results = pd.DataFrame({
                            'image_name': [],
                            'class_id': [],
                            'pointing': [],
                            'pointing_difficult': [],
                        })
        self.im_ix = -1
    def _lazy_init(self):
        if self.device is not None:
            return

        if self.log:
            from torchray.benchmark.logging import mongo_connect
            self.db = mongo_connect(self.experiment.series)

#         self.device = get_device()
        if self.device_preference == 'cpu':
            self.device  = 'cpu'
        else:
            self.device  = 'cuda' if torch.cuda.is_available() else 'cpu'
        
            
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False
        if True:
            # from torchray.benchmark.models import get_model
            # from benchmark.architectures import get_model
            from architectures import get_model
            self.model = get_model(
                arch=self.experiment.arch,
                dataset=self.experiment.dataset,
                convert_to_fully_convolutional=True,
            )
        else:
            print('bypassing torchray get model')
            self.model = torchvision.models.vgg16(pretrained=True)
        
        # Some methods require patching the models further for
        # optimal performance.
        if self.experiment.arch == 'resnet50':
            if any([e in self.experiment.method for e in [
                    'contrastive_excitation_backprop',
                    'deconvnet',
                    'excitation_backprop',
                    'grad_cam',
                    'gradient',
                    'guided_backprop'
            ]]):
                self.model.avgpool = torch.nn.AvgPool2d((7, 7), stride=1)

            if 'excitation_backprop' in self.experiment.method:
                # Replace skip connection with EltwiseSum.
                from torchray.attribution.excitation_backprop import update_resnet
                self.model = update_resnet(self.model, debug=True)
        
        if False:
            self.model = torch.nn.DataParallel(self.model)
        # Change model to eval modself.
        self.model.eval()

        # Move model to device.
        self.model.to(self.device)
        # import pdb;pdb.set_trace()

    def __iter__(self):
        return self

    def __len__(self):
        return len(self.loader.dataset)

    def __next__(self):
        self.im_ix += 1
        self._lazy_init()
        x, y = next(self.data_iterator)
        # import pdb;pdb.set_trace()
        self.current_ix += 1
        if self.start_from is not None:
            if self.current_ix < self.start_from:
                '''
                results['pointing'][class_id] = self.pointing.evaluate(
                    y[0], class_id, point[0])
                results['pointing_difficult'][class_id] = se
                lf.pointing_difficult.evaluate(
                    y[0], class_id, point[0])
                '''
                return {}

        torch.manual_seed(self.seed)


        #...................................
        if self.log:
            from torchray.benchmark.logging import mongo_load, mongo_save, \
                data_from_mongo, data_to_mongo

        '''
        try:
        '''
        if True:
            assert len(x) == 1
            x = x.to(self.device)
            class_ids = self.data.as_class_ids(y[0])
            image_size = self.data.as_image_size(y[0])

            results = {
                'pointing': {},
                'pointing_difficult': {}
            }
            info = {}
            rise_saliency = None
            all_class_ids = class_ids
            class_ix_and_id = enumerate(all_class_ids)
            if self.difficulty:
                assert len(y)==1
                image_filenameroot = y[0]['annotation']['filenameroot'] #TODO: check if this truly is a difficult image
                difficult_class_ids = self.difficulty[image_filenameroot]
                class_ix_and_id = [(class_ix,class_id) for class_ix,class_id in enumerate(all_class_ids) if difficult_class_ids[class_id]]
                class_ids = [id_ for ix,id_ in class_ix_and_id]
            # import pdb;pdb.set_trace() #TODO: check if the class_ix,class_id machinery is working well
            
            for class_ix,class_id in class_ix_and_id:
                if (self.im_ix < 2) or all( [
                    # False,
                    # (self.im_ix == 2) , 
                    (class_id == 8)

                    # (self.im_ix == 3) , 
                    # (class_id == 8) or (class_id == 10)
                    #------------------------------
                    # False
                    ]):
                    print('HACK: ignoring some images')
                    results['pointing'][class_id] = 1
                    results['pointing_difficult'][class_id] = 1        
                    continue
                # Try to recover this result from the log.
                if self.log > 0:
                    image_name = self.data.as_image_name(y[0])
                    data = mongo_load(
                        self.db,
                        self.experiment.name,
                        f"{image_name}-{class_id}",
                    )
                    if data is not None:
                        data = data_from_mongo(data)
                        results['pointing'][class_id] = data['pointing']
                        results['pointing_difficult'][class_id] = data['pointing_difficult']
                                
                        if self.debug:
                            print(f'{image_name}-{class_id} loaded from log')
                        continue

                #...................................
                # class_name = y[0]['annotation']['object'][class_ix]['name']
                # from benchmark.dataset import voc_classes
                from dataset import voc_classes
                class_name = voc_classes.class_names[class_id]

                assert len(y) == 1
                imname = y[0]['annotation']['filename'].split('.')[0]        
                
                full_imname = f'{class_name}{imname}'
                im_save_dir = f'{self.OPTIONS["save_dir"]}/{full_imname}'
                #............................................
                info = None
                if os.path.exists(os.path.join(im_save_dir,'results.pkl')):
                    with open(os.path.join(im_save_dir,'results.pkl'),'rb') as f:
                        try:
                            info = pickle.load(f)
                        except EOFError as e:
                            info = None
                if info is not None:
                    point = [info['COM']]
                    results['pointing'][class_id] = self.pointing.evaluate(
                                    y[0], class_id, point[0])
                    results['pointing_difficult'][class_id] = self.pointing_difficult.evaluate(
                        y[0], class_id, point[0])

                    if self.log > 0:
                        image_name = self.data.as_image_name(y[0])
                        mongo_save(
                            self.db,
                            self.experiment.name,
                            f"{image_name}-{class_id}",
                            data_to_mongo({
                                'image_name': image_name,
                                'class_id': class_id,
                                'pointing': results['pointing'][class_id],
                                'pointing_difficult': results['pointing_difficult'][class_id],
                            })
                        )
                    continue
                #......................
                # TODO(av): should now be obsolete
                if x.grad is not None:
                    x.grad.data.zero_()

                if self.experiment.method == "center":
                    w, h = image_size
                    point = torch.tensor([[w / 2, h / 2]])

                elif self.experiment.method == "gradient":
                    saliency = gradient(
                        self.model, x, class_id,
                        resize=image_size,
                        smooth=0.02,
                        get_backward_gradient=get_pointing_gradient
                    )
                    point = _saliency_to_point(saliency)
                    info['saliency'] = saliency

                elif self.experiment.method == "deconvnet":
                    saliency = deconvnet(
                        self.model, x, class_id,
                        resize=image_size,
                        smooth=0.02,
                        get_backward_gradient=get_pointing_gradient
                    )
                    point = _saliency_to_point(saliency)
                    info['saliency'] = saliency

                elif self.experiment.method == "guided_backprop":
                    saliency = guided_backprop(
                        self.model, x, class_id,
                        resize=image_size,
                        smooth=0.02,
                        get_backward_gradient=get_pointing_gradient
                    )
                    point = _saliency_to_point(saliency)
                    info['saliency'] = saliency

                elif self.experiment.method == "grad_cam":
                    saliency = grad_cam(
                        self.model, x, class_id,
                        saliency_layer=self.gradcam_layer,
                        resize=image_size,
                        get_backward_gradient=get_pointing_gradient
                    )
                    point = _saliency_to_point(saliency)
                    # assert False
                    # info['saliency'] = saliency
                    from matplotlib import pyplot as plt
                    plt.figure()
                    plt.imshow(tensor_to_numpy(saliency)[0,0])
                    plt.show()

                elif self.experiment.method == "excitation_backprop":
                    saliency = excitation_backprop(
                        self.model, x, class_id, self.saliency_layer,
                        resize=image_size,
                        get_backward_gradient=get_pointing_gradient
                    )
                    point = _saliency_to_point(saliency)
                    info['saliency'] = saliency

                elif self.experiment.method == "contrastive_excitation_backprop":
                    saliency = contrastive_excitation_backprop(
                        self.model, x, class_id,
                        saliency_layer=self.saliency_layer,
                        contrast_layer=self.contrast_layer,
                        resize=image_size,
                        get_backward_gradient=get_pointing_gradient
                    )
                    point = _saliency_to_point(saliency)
                    info['saliency'] = saliency

                elif self.experiment.method == "rise":
                    # For RISE, compute saliency map for all classes.
                    if rise_saliency is None:
                        rise_saliency = rise(self.model, x, resize=image_size, seed=self.seed)
                    saliency = rise_saliency[:, class_id, :, :].unsqueeze(1)
                    point = _saliency_to_point(saliency)
                    info['saliency'] = saliency

                elif self.experiment.method == "extremal_perturbation":

                    if self.experiment.dataset == 'voc_2007':
                        areas = [0.025, 0.05, 0.1, 0.2]
                    else:
                        areas = [0.018, 0.025, 0.05, 0.1]

                    if self.experiment.boom:
                        raise RuntimeError("BOOM!")
                    # import pdb;pdb.set_trace()
                    mask, energy = elp.extremal_perturbation(
                        self.model, x, class_id,
                        areas=areas,
                        num_levels=8,
                        step=7,
                        sigma=7 * 3,
                        max_iter=800,
                        debug=self.debug > 0,
                        jitter=True,
                        smooth=0.09,
                        resize=image_size,
                        perturbation='blur',
                        reward_func=elp.simple_reward,
                        variant=elp.PRESERVE_VARIANT,
                    )

                    saliency = mask.sum(dim=0, keepdim=True)
                    point = _saliency_to_point(saliency)

                    info = {
                        'saliency': saliency,
                        'mask': mask,
                        'areas': areas,
                        'energy': energy
                    }
                    from matplotlib import pyplot as plt
                    import kornia as K
                    plt.figure()
                    plt.imshow(tensor_to_numpy(
                        K.geometry.transform.resize(mask,ref.shape[-2:])
                        )[0,0])
                    plt.show()
                    import time;time.sleep(5)
                    import sys;sys.exit()

                elif self.experiment.method == 'multithresh_extremal_perturbation':
                    n_areas = 40
                    mask, energy = multithresh_elp.extremal_perturbation(
                        self.model, x, class_id,
                        n_areas=n_areas,
                        num_levels=8,
                        step=7,
                        sigma=7 * 3,
                        max_iter=201,
                        debug_flag =self.debug > 0,
                        jitter=True,
                        smooth=0.09,
                        resize=image_size,
                        perturbation='blur',
                        reward_func=elp.simple_reward,
                        variant=elp.PRESERVE_VARIANT,
                    )

                    saliency = mask.sum(dim=0, keepdim=True)
                    point = _saliency_to_point(saliency)

                    info = {
                        'saliency': saliency,
                        'mask': mask,
                        'n_areas': n_areas,
                        'energy': energy
                    }
                    from matplotlib import pyplot as plt
                    import kornia as K
                    plt.figure()
                    plt.imshow(tensor_to_numpy(
                        K.geometry.transform.resize(mask,ref.shape[-2:])
                        )[0,0])
                    plt.show()
                    import time;time.sleep(5)
                    import sys;sys.exit()
                    if True:
                        del info; import gc;gc.collect()
                        torch.cuda.empty_cache()                    

                else:
                    assert False

                results['pointing'][class_id] = self.pointing.evaluate(
                    y[0], class_id, point[0])
                results['pointing_difficult'][class_id] = self.pointing_difficult.evaluate(
                    y[0], class_id, point[0])
                #=========================================================
                # because i'm more comfortable with pandas
                image_name = self.data.as_image_name(y[0])
                self.df_results = self.df_results.append({
                        'image_name': image_name,
                        'class_id': class_id,
                        'pointing': results['pointing'][class_id],
                        'pointing_difficult': results['pointing_difficult'][class_id],
                    },   ignore_index=True)
                self.df_results.to_csv(os.path.join(self.OPTIONS['save_dir'],f'results{str(self.pid) if (self.pid is not None) else ""}.csv'))
                #=========================================================
                if self.log > 0:
                    image_name = self.data.as_image_name(y[0])
                    mongo_save(
                        self.db,
                        self.experiment.name,
                        f"{image_name}-{class_id}",
                        data_to_mongo({
                            'image_name': image_name,
                            'class_id': class_id,
                            'pointing': results['pointing'][class_id],
                            'pointing_difficult': results['pointing_difficult'][class_id],
                        })
                    )

                if self.log > 1:
                    mongo_save(
                        self.db,
                        str(self.experiment.name) + "-details",
                        f"{image_name}-{class_id}",
                        data_to_mongo(info)
                    )
            # import sys;sys.exit()
            return results
        '''
        except Exception as ex:
            raise ProcessingError(
                self, self.experiment, self.model, x, y, class_id, image_size) from ex
        '''
    def aggregate(self, results):
        for class_id, hit in results['pointing'].items():
            self.pointing.aggregate(hit, class_id)
        for class_id, hit in results['pointing_difficult'].items():
            self.pointing_difficult.aggregate(hit, class_id)
    
    def run(self, save=True):
        
        all_results = []
        for itr, results in enumerate(tqdm(self)):
            with Timer.get('each-saliency-run') as tim:
                print(colored(f'{itr} done' ,'green'))
                if self.start_from is not None:
                    if len(results) == 0:
                        continue
                all_results.append(results)
                self.aggregate(results)
    #             if itr % max(len(self) // 20, 1) == 0 or itr == len(self) - 1:
                if True: #NOTE: print after every image
                    print("[{}/{}]".format(itr + 1, len(self)))
                    print(self)
                
        print("[final result]")
        print(self)

        self.experiment.pointing = self.pointing.accuracy
        self.experiment.pointing_difficult = self.pointing_difficult.accuracy
        if save:
            self.experiment.save()

        return all_results,self.df_results

    def __str__(self):
        return (
            f"{self.experiment.method} {self.experiment.arch} "
            f"{self.experiment.dataset} "
            f"pointing_game: {self.pointing}\n"
            f"{self.experiment.method} {self.experiment.arch} "
            f"{self.experiment.dataset} "
            f"pointing_game(difficult): {self.pointing_difficult}"
        )

#=======================================================================
#
#=======================================================================

class Experiment():
    def __init__(self,
                 series,
                 method,
                 arch,
                 dataset,
                 root='',
                 chunk=None,
                 boom=False):
        self.series = series
        self.root = root
        self.method = method
        self.arch = arch
        self.dataset = dataset
        self.chunk = chunk
        self.boom = boom
        self.pointing = float('NaN')
        self.pointing_difficult = float('NaN')

    def __str__(self):
        return (
            f"{self.method},{self.arch},{self.dataset},"
            f"{self.pointing:.5f},{self.pointing_difficult:.5f}"
        )

    @property
    def name(self):
        name = f"{self.method}-{self.arch}-{self.dataset}"
        if self.chunk is not None:
            name = f"{name}-{self.chunk}"
        return name

    @property
    def path(self):
        return os.path.join(self.root, self.name + ".csv")

    def save(self):
        print('skipping torchray save')
        if False:
            with open(self.path, "w") as f:
                f.write(self.__str__() + "\n")

    def load(self):
        with open(self.path, "r") as f:
            data = f.read()
        method, arch, dataset, pointing, pointing_difficult = data.split(",")
        assert self.method == method
        assert self.arch == arch
        assert self.dataset == dataset
        self.pointing = float(pointing)
        self.pointing_difficult = float(pointing_difficult)

    def done(self):
        return os.path.exists(self.path)

#=======================================================================
#
#=======================================================================
    
    
class ProcessingError(Exception):
    def __init__(self, executor, experiment, model, image, label, class_id, image_size):
        super().__init__(f"Error processing {str(label):20s}")
        self.executor = executor
        self.experiment = experiment
        self.model = model
        self.image = image
        self.label = label
        self.class_id = class_id
        self.image_size = image_size
        
#=======================================================================
#
#=======================================================================

        
def _saliency_to_point(saliency):
    assert len(saliency.shape) == 4
    w = saliency.shape[3]
    point = torch.argmax(
        saliency.view(len(saliency), -1),
        dim=1,
        keepdim=True
    )
    return torch.cat((point % w, point // w), dim=1)
