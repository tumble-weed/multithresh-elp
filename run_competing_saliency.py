#%%
# import os
# os.chdir('/root/evaluate-saliency-4/jigsaw')    
import torch
import torchvision
import numpy as np
from benchmark.benchmark_utils import ChangeDir,AddPath
import skimage.io
from PIL import Image
from cnn import get_target_id
import os
from pydoc import importfile
#%% gradcam
from benchmark.benchmark_utils import create_im_save_dir
import os
import pickle
from benchmark import settings


with AddPath('benchmark/pytorch_grad_cam') as ctx:
# if True:
    # import sys;sys.path.append('benchmark/pytorch_grad_cam')
    from pytorch_grad_cam import GradCAM
    from pytorch_grad_cam.utils.model_targets import ClassifierOutputTarget
    def run_gradcam(model,ref,target_id):
        # https://github.com/zhuokaizhao/Grad-CAM-VGG19-PyTorch/blob/06f33dbe8519bd0dccc65e50ffe78e74673e6769/model.py#L13
    # https://medium.com/@stepanulyanin/implementing-grad-cam-in-pytorch-ea0937c31e82
        if len(model.features) == 37:
            target_layers = [model.features[35]]
        elif len(model.features) == 31:
            target_layers = [model.features[29]]
        else:
            # import pdb;pdb.set_trace()
            assert False
        input_tensor = ref#get_image_tensor(P.impath,size=P.size).to(P.device)
        targets = [ClassifierOutputTarget(target_id)]
        with GradCAM(model=model, target_layers=target_layers) as cam:
            grayscale_cams = cam(input_tensor=input_tensor, targets=targets)
        return {
                'saliency':grayscale_cams,
                'target_id':target_id,
                'method':'gradcam',
            }
        # im_save_dir = create_im_save_dir(experiment_name=f'gradcam-{modelname}',impath=impath)  
        # savename = os.path.join(im_save_dir,f'{classname}{target_id}.pkl')
        # with open(savename,'wb') as f:
        #     pickle.dump(,f)            
##############################################################################################
#%% smoothgrad
from benchmark.benchmark_utils import create_im_save_dir
import pickle
import glob
# with ChangeDir('benchmark/fullgrad-saliency') as ctx:
with AddPath('benchmark/fullgrad-saliency') as ctx:
    from saliency.smoothgrad import SmoothGrad
    def run_smoothgrad(model,ref,target_id):
        image = ref
        device = ref.device
        smooth_grad = SmoothGrad(model, num_samples=100, std_spread=0.15)
        smoothgrad_saliency = smooth_grad.saliency(image, target_class=torch.tensor([target_id]).long().to(device))
        return {
                        'saliency':smoothgrad_saliency,
                        'target_id':target_id,
                        # 'class_name':classname,
                        # 'impath':impath,
                        'method':'smoothgrad',                
                        }
        
        im_save_dir = create_im_save_dir(experiment_name=f'smoothgrad-{modelname}',impath=impath)  
        savename = os.path.join(im_save_dir,f'{classname}{target_id}.pkl')
        with open(savename,'wb') as f:
            pickle.dump({
                        'saliency':smoothgrad_saliency,
                        'target_id':target_id,
                        'class_name':classname,
                        'impath':impath,
                        'method':'smoothgrad',                
                        },f)
##############################################################################################
#%% fullgrad
from benchmark.benchmark_utils import create_im_save_dir
import pickle
import glob
# with ChangeDir('benchmark/fullgrad-saliency') as ctx:
with AddPath('benchmark/fullgrad-saliency') as ctx:
    from saliency.fullgrad import FullGrad
    full_grad_assets = []
    def run_fullgrad(model,ref,target_id,full_grad_assets=full_grad_assets):
        #######################################################################################
        # smoothgrad
        image = ref
        device = ref.device
        if len(full_grad_assets) == 0:
            full_grad = FullGrad(model, im_size = image.shape[1:] )
            full_grad_assets.append(full_grad)
        full_grad = full_grad_assets[0]
        done = False
        while not done:
            try:
                fullgrad_saliency = full_grad.saliency(image, target_class=torch.tensor([target_id]).long().to(device))
                done = True
            except AssertionError as e:
                pass
        
            
        return {
                        'saliency':fullgrad_saliency,
                        'target_id':target_id,
                        'method':'fullgrad',
                        }
        # im_save_dir = create_im_save_dir(experiment_name=f'fullgrad-{modelname}',impath=impath)  
        # savename = os.path.join(im_save_dir,f'{classname}{target_id}.pkl')
        # with open(savename,'wb') as f:
        #     pickle.dump({
        #                 'saliency':fullgrad_saliency,
        #                 'target_id':target_id,
        #                 'class_name':classname,
        #                 'impath':impath,
        #                 'method':'fullgrad',
        #                 },f)
        #######################################################################################
##############################################################################################
#%% integrated gradients
from benchmark.benchmark_utils import create_im_save_dir
from captum.attr import IntegratedGradients
def run_integrated_gradients(model,ref,target_id):
    integrated_gradients = IntegratedGradients(model,multiply_by_inputs=False)
    image = ref
    device = ref.device
    attributions_ig = integrated_gradients.attribute(ref, target=target_id, n_steps=100)
    #######################################################################################
    # visualization
    if False:
        from captum.attr import visualization as viz
        from matplotlib.colors import LinearSegmentedColormap

        default_cmap = LinearSegmentedColormap.from_list('custom blue', 
                                                         [(0, '#ffffff'),
                                                          (0.25, '#000000'),
                                                          (1, '#000000')], N=256)

        _ = viz.visualize_image_attr(np.transpose(attributions_ig.squeeze().cpu().detach().numpy(), (1,2,0)),
                                     np.transpose(ref.squeeze().cpu().detach().numpy(), (1,2,0)),
                                     method='heat_map',
                                     cmap=default_cmap,
                                     show_colorbar=True,
                                     sign='positive',
                                     outlier_perc=1)    

    return {
                    'saliency':attributions_ig,
                    'target_id':target_id,
                    'method':'integrated-gradients',
                    }
    # im_save_dir = create_im_save_dir(experiment_name=f'IG-{modelname}',impath=impath)  
    # savename = os.path.join(im_save_dir,f'{classname}{target_id}.pkl')
    # with open(savename,'wb') as f:
    #     pickle.dump({
    #                 'saliency':attributions_ig,
    #                 'target_id':target_id,
    #                 'class_name':classname,
    #                 'impath':impath,
    #                 'method':'integrated-gradients',
    #                 },f)

##############################################################################################
#%% gradients
from benchmark.benchmark_utils import create_im_save_dir
from captum.attr import Saliency
def run_gradients(model,ref,target_id):
    gradients = Saliency(model)
    image = ref
    device = ref.device
    attributions_g = gradients.attribute(ref, target=target_id)   
    #######################################################################################
    # visualization
    if False:
        from captum.attr import visualization as viz
        from matplotlib.colors import LinearSegmentedColormap

        default_cmap = LinearSegmentedColormap.from_list('custom blue', 
                                                         [(0, '#ffffff'),
                                                          (0.25, '#000000'),
                                                          (1, '#000000')], N=256)

        _ = viz.visualize_image_attr(np.transpose(attributions_g.squeeze().cpu().detach().numpy(), (1,2,0)),
                                     np.transpose(ref.squeeze().cpu().detach().numpy(), (1,2,0)),
                                     method='heat_map',
                                     cmap=default_cmap,
                                     show_colorbar=True,
                                     sign='positive',
                                     outlier_perc=1)  
    return {
                    'saliency':attributions_g,
                    'target_id':target_id,
                    'method':'gradients',
                    }                                     
    #######################################################################################       
    # from benchmark.benchmark_utils import create_im_save_dir
    # im_save_dir = create_im_save_dir(experiment_name=f'gradients-{modelname}',impath=impath)  
    # savename = os.path.join(im_save_dir,f'{classname}{target_id}.pkl')
    # with open(savename,'wb') as f:
    #     pickle.dump({
    #                 'saliency':attributions_g,
    #                 'target_id':target_id,
    #                 'class_name':classname,
    #                 'impath':impath,
    #                 'method':'gradients',
    #                 },f)
##############################################################################################
#%% input X gradients
from benchmark.benchmark_utils import create_im_save_dir
from captum.attr import InputXGradient
from benchmark.benchmark_utils import create_im_save_dir
def run_inputXgradients(model,ref,target_id):
    input_x_gradients = InputXGradient(model)
    device = ref.device
    attributions_ixg = input_x_gradients.attribute(ref, target=target_id)
    #######################################################################################
    # visualization
    if False:
        from captum.attr import visualization as viz
        from matplotlib.colors import LinearSegmentedColormap

        default_cmap = LinearSegmentedColormap.from_list('custom blue', 
                                                         [(0, '#ffffff'),
                                                          (0.25, '#000000'),
                                                          (1, '#000000')], N=256)

        _ = viz.visualize_image_attr(np.transpose(attributions_ixg.squeeze().cpu().detach().numpy(), (1,2,0)),
                                     np.transpose(ref.squeeze().cpu().detach().numpy(), (1,2,0)),
                                     method='heat_map',
                                     cmap=default_cmap,
                                     show_colorbar=True,
                                     sign='positive',
                                     outlier_perc=1)
    #######################################################################################
    return {
                    'saliency':attributions_ixg,
                    'target_id':target_id,
                    'method':'inputXgradient',
                    }

    # im_save_dir = create_im_save_dir(experiment_name=f'inputXgradient-{modelname}',impath=impath)  
    # savename = os.path.join(im_save_dir,f'{classname}{target_id}.pkl')
    # with open(savename,'wb') as f:
    #     pickle.dump({
    #                 'saliency':attributions_ixg,
    #                 'target_id':target_id,
    #                 'class_name':classname,
    #                 'impath':impath,
    #                 'method':'inputXgradient',
    #                 },f)
##############################################################################################
##############################################################################################
#%% jigsaw_saliency

# https://stackoverflow.com/questions/51186921/how-to-patch-a-python-3-module-on-first-import
#----------------------------------------------------------------------
import builtins
original_import = builtins.__import__

def custom_import(*args, **kw):
    module = original_import(*args, **kw)

    if (module.__name__ == "jigsaw" 
        and not getattr(module, "patch_is_performed", False)):
#         patch(module)
        import jigsaw2 as module
#         import jigsaw_pyr_blend as module
        module.patch_is_performed = True

    return module

builtins.__import__ = custom_import
#----------------------------------------------------------------------
def patch_import():
    #====================================================================
    # overriding import for jigsaw
    # https://stackoverflow.com/questions/51186921/how-to-patch-a-python-3-module-on-first-import
    import builtins
    original_import = builtins.__import__

    def custom_import(*args, **kw):
        module = original_import(*args, **kw)
        # print(module.__name__)
        # import pdb;pdb.set_trace()
        if (module.__name__ == "jigsaw" 
            and not getattr(module, "patch_is_performed", False)):
            #-------------------------------
            import jigsaw2 as module
            print('patching jigsaw2')
            #-------------------------------
    #         import jigsaw3 as module
    #         print('patching jigsaw3')        
            #-------------------------------
    #         import jigsaw_pyr_blend as module
            module.patch_is_performed = True
        
        if False and (module.__name__ == "augmentation" 
            and not getattr(module, "patch_is_performed", False)):
            #-------------------------------
    #         import augmentation_with_inversion as module
    #         print('patching augmentation_with_inversion')
            #-------------------------------
        
            module.patch_is_performed = True
            
        if (module.__name__ == "metrics" 
            and not getattr(module, "patch_is_performed", False)):

    #         import metrics2 as module
    #         print('patching metrics2')

            module.patch_is_performed = True
        if (module.__name__ == "importance" 
            and not getattr(module, "patch_is_performed", False)):
            import importance_with_bg as module
            print('patching importance_with_bg')        
            module.patch_is_performed = True

        return module
    builtins.__import__ = custom_import
    #====================================================================    

from main import part_discovery
import inspect
tensor_to_numpy = lambda t:t.detach().cpu().numpy()
def run_jigsaw_saliency(model,ref,target_id,device=None):
    patch_import()
    import parameters.parameters6 as P
    assert P.jigsaw_options['paste_on'] == 'image'
    assert P.niter1 == 100
    assert P.patch_options['n_patches'] == 49

    P.class_name = None
    P.niter1 = 100
    P.lr = 1e0*3/224
    if device is not None:
        P.device = device
    P.evaluation_options['visualize_every'] = None
    P.evaluation_options['evaluate_every'] = None
    trends,patch_theta0,importances = part_discovery(
            P.patch_options,
            P.jigsaw_options,
            ref,model,target_id,
            P.niter1,P.niter2,P.lr,P.pixel_jitter,
            P.patch_correction_mode,
            P.evaluation_options,
            P.loss_options,
            P.importance_options,
            P.non_lin,P.patch_lim_mode,
            device=P.device)
    #===========================================
    P_dict = {}
    for name in dir(P):
        if any([
            name.startswith("__"),
#             isinstance(item,ModuleType),
            inspect.ismodule(P.__dict__[name]),
        ]):
            continue
        P_dict[name] = P.__dict__[name]
    # print(P_dict)
    #===========================================                
    return {
            'parameters':P_dict,
            'ref':tensor_to_numpy(ref),
            'trends':trends,
            'patch_theta0':tensor_to_numpy(patch_theta0),
            'importances':(importances).__getstate__(),
            'target_id':target_id,
            'saliency':trends['im_overlayed_importances'],
            }
#%% run
import pickle
import glob
# from utils import get_image_tensor
def get_image_tensor(impath,size=224):
    from cnn import get_vgg_transform
    vgg_transform = get_vgg_transform(size)
    im_ = skimage.io.imread(impath)
    if im_.ndim == 2:
        im_ = np.concatenate([im_[...,None],im_[...,None],im_[...,None]],axis=-1)
    im_pil = Image.fromarray(im_)
    ref = vgg_transform(im_pil).unsqueeze(0)
    return ref
#%%
def get_hack_for_im_save_dirs(methodname,modelname):
    save_dir = os.path.join(settings.RESULTS_DIR,f'{methodname}-{modelname}')
    keep_imroots = glob.glob(os.path.join(save_dir,'*/'))
    keep_imroots = [ os.path.basename(n.rstrip(os.path.sep)) for n in keep_imroots]
    keep_imroots = [ ( n[:-len('.JPEG')] if '.' in n else n) for n in keep_imroots]
    
    def hack_for_im_save_dirs(im_save_dirs):
        out_im_save_dirs = []
        
        for im_save_dir in im_save_dirs:
            imroot = os.path.basename(im_save_dir.rstrip(os.path.sep))    
            imroot =  ( imroot[:-len('.JPEG')] if '.' in imroot else imroot)
            # impath = os.path.join(imagenet_root,'images','val',imroot + '.JPEG')
            if imroot in keep_imroots:
                out_im_save_dirs.append(im_save_dir)
            # import pdb;pdb.set_trace()
        # assert len(out_im_save_dirs) == len(keep_imroots)
        return out_im_save_dirs 
    
    return hack_for_im_save_dirs

#%%
available_methodnames = [  
                # 'gradcam',
                # 'smoothgrad',
                # 'fullgrad',
                # 'integrated-gradients',
                # 'gradients',
                # 'inputXgradients'
                'jigsaw-saliency'
                ]
def main(    
    methodnames = available_methodnames,
    skip=False,
    start = 0,
    end=None,
    device = 'cuda'):
# def main():
    modelname = 'vgg16'
    imagenet_root = '/root/evaluate-saliency-4/jigsaw/imagenet'
    model = torchvision.models.__dict__[modelname](pretrained=True).to(device)
    model.eval()
    # impaths = ['/root/evaluate-saliency-4/cyclegan results/samoyed1.png'] 
    # class_name = 'samoyed'
    imagenet_root = '/root/evaluate-saliency-4/jigsaw/imagenet'
    image_paths = sorted(glob.glob(os.path.join(imagenet_root,'images','val','*.JPEG')))
    size=(224)
    image_paths = list((image_paths))
    if end is not None:
        image_paths = image_paths[:end]
    image_paths = image_paths[start:]
    print(start,end)
    # assert False
    ##############################################################################################

    for methodname in methodnames:
        if True:
            hack_for_im_save_dirs = get_hack_for_im_save_dirs('jigsaw-saliency',modelname)
            print('#'*50,'\n','hack_for_im_save_dirs','\n','#'*50) 
            # import pdb;pdb.set_trace()   
            image_paths = hack_for_im_save_dirs(image_paths)                
        for i,impath in enumerate(image_paths):
            # if 'skip2' and (i < 1750):
            #     continue
        # for impath in image_paths[1750:]:
            # if impath.endswith('ILSVRC2012_val_00000005.JPEG'):
            #     import pdb;pdb.set_trace()

            imroot = os.path.basename(impath).split('.')[0]
        #     bboxpath = os.path.join(imagenet_root,'bboxes','val',imroot + '.xml')    
            import imagenet_localization_parser
            from benchmark.synset_utils import get_synset_id,synset_id_to_imagenet_class_ix
            bbox_info = imagenet_localization_parser.get_voc_label(
                root_dir = os.path.join(imagenet_root,'bboxes','val'),
                x = imroot)
        #     print(bbox_info)
            synset_id = bbox_info['annotation']['object'][0]['name']
        #     print(synset_id)
            target_id = synset_id_to_imagenet_class_ix(synset_id)
        #     print(target_id)
            import imagenet_synsets
            classname = imagenet_synsets.synsets[target_id]['label']
            classname = classname.split(',')[0]
            if False:
                classname = '_'.join(classname)
            else:
                classname = classname.replace(' ','_')
            #-------------------------------------------------------------------
            if skip:
                # assert False,'only skips if integrated-gradients'
                from benchmark import settings
                imroot = os.path.split(impath)[-1].split('.')[0]
                im_save_dir = os.path.join(settings.RESULTS_DIR,f'{methodname}-{modelname}',imroot)
                pklname = os.path.join(im_save_dir,f'{classname}{target_id}.pkl')
                # import pdb;pdb.set_trace()
                if os.path.exists(pklname):
                    # import pdb;pdb.set_trace()
                    print(f'{pklname} exists, skipping')
                    continue
            #-------------------------------------------------------------------

            ref = get_image_tensor(impath,size=(224,)).to(device)


            '''
            from pytorch_grad_cam.metrics.road import ROADMostRelevantFirstAverage,ROADLeastRelevantFirstAverage
            '''
            #######################################################################################
            if methodname == 'gradcam':
                saliency_data = run_gradcam(model,ref,target_id)        
            if methodname == 'smoothgrad':
                saliency_data = run_smoothgrad(model,ref,target_id)
            if methodname == 'fullgrad':
                saliency_data = run_fullgrad(model,ref,target_id)
            if methodname == 'integrated-gradients':
                try:
                    saliency_data = run_integrated_gradients(model,ref,target_id)
                except RuntimeError as e:
                    print(e)
            if methodname == 'gradients':
                saliency_data = run_gradients(model,ref,target_id)
            if methodname == 'inputXgradients':
                saliency_data = run_inputXgradients(model,ref,target_id)
            if methodname == 'jigsaw-saliency':
                saliency_data =run_jigsaw_saliency(model,ref,target_id,device=device)
            saliency_data.update(
                dict(
                    modelname = modelname,
                    impath = impath,
                    classname = classname
                )
            )            
            #.......................................................
            im_save_dir = create_im_save_dir(experiment_name=f'{methodname}-{modelname}',impath=impath)  
            savename = os.path.join(im_save_dir,f'{classname}{target_id}.pkl')
            with open(savename,'wb') as f:
                pickle.dump(saliency_data,f)        

    ##############################################################################################
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--methodnames',nargs='*',default = [  
                    # 'gradcam',
                    # 'smoothgrad',
                    # 'fullgrad',
                    'integrated-gradients',
                    # 'gradients',
                    # 'inputXgradients',
                    # 'jigsaw-saliency'
                    ])
    parser.add_argument('--skip',type=lambda v: (v.lower()=='true'),default=False)
    parser.add_argument('--start',type=int,default=0)
    parser.add_argument('--end',type=int,default=None)
    parser.add_argument('--device',type=str,default=None)
    args = parser.parse_args()
    # import pdb;pdb.set_trace()
    # main(methodnames=args.methodnames,skip=args.skip)    
    main(skip=args.skip,start=args.start,end=args.end,device=args.device,methodnames=args.methodnames)
