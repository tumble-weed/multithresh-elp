import benchmark.settings as settings


def setup_chunk(
    base_parameters = 'parameters.parameters_vgg16_voc2007',
    ):
    import importlib
    parameters = importlib.import_module(base_parameters)    
    return vars(parameters)
    

IMAGES = {
    '009183':'cat',
    '000001':['person','dog'][1],
    '006653':['car'][0],
    '000003':['sofa'][0],
    '007500':['person','bottle'][1],
    '000006':['chair'][0],
    }
def setup_single_image(
    base_parameters = 'parameters.parameters_vgg16_voc_may18',
    relative_save_dir = 'debug_saliency',
    image_name = '007500',
    class_name = None):
    import os
    import skimage.io
    from PIL import Image
    # from image_transforms import get_transform
    from cnn import get_vgg_transform
    #--------------------------------
    parameters = setup_chunk(
        base_parameters = base_parameters,
        )    
    #--------------------------------
    OPTIONS = parameters['OPTIONS']
    device = parameters['meta_OPTIONS']['device']
    parameters['OPTIONS']['save_dir'] = os.path.join(settings.RESULTS_DIR,relative_save_dir)
    PLOT = False
    #--------------------------------
    dataset_name = parameters['meta_OPTIONS']['datasets'][0]
    assert dataset_name.startswith('voc'), 'coco not implemented'
    voc_transform = get_vgg_transform(size=(224,))
    #--------------------------------
    from benchmark.dataset.voc_classes import class_names    
    #===========================================
    im_dir = os.path.join(
        os.path.dirname(os.path.dirname(settings.RESULTS_DIR)),
        'VOCdevkit/VOC2007/JPEGImages')
    imroot = image_name
    if class_name is None:
        assert imroot in IMAGES
        class_name = IMAGES[imroot]

    class_id = class_names.index(class_name)
    #===========================================
    impath = os.path.join(im_dir,imroot+'.jpg')

    '''
    from coco_classes import class_names
    coco_root = 'data/datasets/coco/val2014'
    class_name = 'banana'
    imroot = 'COCO_val2014_000000113113'
    if imroot == 'COCO_val2014_000000113113':
        class_id = class_names.index(class_name)
    impath = os.path.join(coco_root,imroot+'.jpg')
    '''
    # imroot = os.path.basename(impath).split('.')[0]
    im_ = skimage.io.imread(impath)
    im_pil = Image.fromarray(im_)
    ref = voc_transform(im_pil).unsqueeze(0)
    #--------------------------------
    from benchmark.architectures import get_model
    arch = parameters['meta_OPTIONS']['archs'][0]
    # arch = 'resnet50'
    model = get_model(
                arch=arch,
                dataset=dataset_name,
                convert_to_fully_convolutional=True,
            )
    model.to(device)
    #--------------------------------
    return {
    'model' : model,
    'ref_full' : ref,
    'class_id' : class_id,
    'class_name' : class_name,
    'OPTIONS' : OPTIONS,
    'PLOT' : PLOT,
    'device' : device,
    'save_dir' : parameters['OPTIONS']['save_dir'],
    }
