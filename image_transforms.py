#=============================================
# logger
#=============================================
# from utils import get_logger_like
# logger = get_logger_like('image_transforms','root',level='ERROR')

#=============================================
'''
vgg_mean = (0.485, 0.456, 0.406)
vgg_std = (0.229, 0.224, 0.225)
denormalize_vgg = lambda t,vgg_mean=vgg_mean,vgg_std=vgg_std:(t * torch.tensor(vgg_std).view(1,3,1,1).to(t.device)) + torch.tensor(vgg_mean).view(1,3,1,1).to(t.device)

'''
#=============================================
# normalizer-denormalizer for torch vgg
#=============================================

import torch
from torchvision import transforms
# device = 'cuda' if torch.cuda.is_available() else 'cpu'
vgg_mean = (0.485, 0.456, 0.406)
vgg_std = (0.229, 0.224, 0.225)
vgg_mean_t = torch.tensor(vgg_mean).view(1,3,1,1)
vgg_std_t = torch.tensor(vgg_std).view(1,3,1,1)

def get_denormalize_vgg():
    denormalize_vgg = lambda t,vgg_mean_t=vgg_mean_t,vgg_std_t=vgg_std_t:(t * vgg_std_t.to(t.device) + vgg_mean_t.to(t.device))
    return denormalize_vgg

def get_normalize_vgg():
    # normalize_vgg = transforms.Normalize(mean=vgg_mean, std=vgg_std)
    normalize_vgg = lambda t,vgg_mean_t=vgg_mean_t,vgg_std_t=vgg_std_t:(t - vgg_mean_t.to(t.device))/vgg_std_t.to(t.device)
    return normalize_vgg

#=============================================
# torchray-resize
#=============================================
from torchvision import transforms
import torch.nn.functional as F

# caffe statistics
bgr_mean = [103.939, 116.779, 123.68]
mean = [m / 255. for m in reversed(bgr_mean)]
std = [1 / 255.] * 3
voc_mean = mean
voc_mean_t = torch.tensor(voc_mean).view(1,3,1,1)
voc_std = std
voc_std_t = torch.tensor(voc_std).view(1,3,1,1)

def get_denormalize_voc():
    denormalize_voc = lambda t,mean=voc_mean,std=voc_std:(t * voc_std_t.to(t.device)) + voc_mean_t.to(t.device)
    return denormalize_voc
# Note: image should always be downsampled. If image is being
# upsampled, then this resize function will not match the behavior
# of skimage.transform.resize in "constant" mode
# (torch.nn.functional.interpolate uses "edge" padding).
'''
def torchray_resize(x,size):
    if not isinstance(size, int):
        orig_height, orig_width = size
    else:
        height, width = x.shape[1:3]
        if width < height:
            orig_width = size
            orig_height = int(size * height / width)
        else:
            orig_height = size
            orig_width = int(size * width / height)
    with torch.no_grad():
        x = F.interpolate(x.unsqueeze(0), (orig_height, orig_width),
                            mode='bilinear', align_corners=False)
        x = x.squeeze(0)
    return x
'''

#==========================================================================
# get_transform
#==========================================================================

import os
import copy
import types
import re

from collections import OrderedDict

import torch
import torch.nn as nn
from torchvision import models, transforms

def get_transform(dataset='imagenet', size=224):
    r"""
    Returns a composition of standard pre-processing transformations for
    feeding models. For non-ImageNet datasets, the transforms are
    for models converted from Caffe (i.e., Caffe pre-processing).
    Args:
        dataset (str): name of dataset, should contain either ``'imagenet'``,
            ``'coco'`` or ``'voc'`` (default: ``'imagenet'``).
        size (sequence or int): desired output size (see
            :class:`torchvision.transforms.Resize` for more details).
    Returns:
        :class:`torchvision.Transform`: transform.
    """
    # Get the data loader transforms.
    if "imagenet" in dataset:
        mean = [0.485, 0.456, 0.406]
        std = [0.229, 0.224, 0.225]

        transform = transforms.Compose([
            transforms.Resize(size),
            transforms.ToTensor(),
            transforms.Normalize(mean=mean, std=std)
        ])

    else:
        # Normalization for Caffe networks.
        bgr_mean = [103.939, 116.779, 123.68]

        # TODO(vedaldi): This legacy code will be removed in the future.
        # # This is exactly the same as pycaffe.
        # from skimage.transform import resize
        # import numpy as np
        # def transform(pil_image):
        #     x = np.array(pil_image).astype(np.float)
        #     h, w = x.shape[:2]
        #     if w < h:
        #         ow = size
        #         oh = int(size * h / w)
        #     else:
        #         oh = size
        #         ow = int(size * w / h)
        #     # This seems unnecessary:
        #     # mn, mx = x.min(), x.max()
        #     # x = (x - mn) / (mx - mn)
        #     # x = resize(x, (oh, ow),
        #     #            order=1,
        #     #            mode='constant',
        #     #            anti_aliasing=False)
        #     # x = x.astype(np.float32) * (mx - mn) + mn
        #     # x = (x - mn) / (mx - mn)
        #     x = resize(x, (oh, ow),
        #                order=1,
        #                mode='constant',
        #                anti_aliasing=False)
        #     x = torch.tensor(x, dtype=torch.float32)
        #     x -= torch.tensor(list(reversed(bgr_mean)), dtype=torch.float32)
        #     x = x.permute([2, 0, 1])
        #     return x

        import torch.nn.functional as F
        mean = [m / 255. for m in reversed(bgr_mean)]
        std = [1 / 255.] * 3

        # Note: image should always be downsampled. If image is being
        # upsampled, then this resize function will not match the behavior
        # of skimage.transform.resize in "constant" mode
        # (torch.nn.functional.interpolate uses "edge" padding).
        def get_resize(size):
            def resize(x):
                # assert False, "check if this is the same as torchray_resize"
                if not isinstance(size, int):
                    orig_height, orig_width = size
                else:
                    height, width = x.shape[1:3]
                    if width < height:
                        orig_width = size
                        orig_height = int(size * height / width)
                    else:
                        orig_height = size
                        orig_width = int(size * width / height)
                with torch.no_grad():
                    x = F.interpolate(x.unsqueeze(0), (orig_height, orig_width),
                                    mode='bilinear', align_corners=False)
                    x = x.squeeze(0)
                return x
            return resize
        transforms_list = [transforms.ToTensor()]
        # import pdb;pdb.set_trace()
        transforms_list = transforms_list + ([get_resize(size)] if size else [])
        transforms_list = transforms_list + [transforms.Normalize(mean=mean, std=std)]

        transform = transforms.Compose(transforms_list)

    return transform